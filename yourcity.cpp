/*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2024  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>


#include "game/Game.h"
#include "game/TextEngine.h"
#include "core/Map.h"



using namespace YourCity;


int main(int argc, char** argv)
{
    auto engine = std::make_unique<TextEngine>();

    auto game = std::make_shared<Game>();

    auto map = std::make_shared<Map>(10, 10, 1);

    map->setZoneType(
        Position(0,0),
        Position(4,3),
        ZoneType::RESIDENTIAL
        );
    map->setZoneType(
        Position(4,4),
        Position(0,7),
        ZoneType::COMMERCIAL
        );
    map->setZoneType(
        Position(4,4),
        Position(8,9),
        ZoneType::INDUSTRIAL
        );
    map->setSurfaceTraffic(
        Position(4,0),
        Position(9,0),
        SurfaceTrafficType::STREET
        );

    game->setMap(map);
    engine->setGame(game);

    engine->run();

    return EXIT_SUCCESS;
}
