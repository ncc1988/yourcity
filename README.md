# Yourcity

Yourcity is an attempt to make a city simulation game in the style
of Sim City 3000.

## Current state

This project is in a very early state where the foundations are being laid.
Currently, the yourcity executable outputs a hardcoded city with a size
of 10 x 10 tiles as CSV.
