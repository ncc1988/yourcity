 /*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2024  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef YOURCITY__TEXTENGINE_H
#define YOURCITY__TEXTENGINE_H


#include <iostream>
#include <string>


#include "Engine.h"


namespace YourCity
{
    /**
     * The TextEngine class provides a text-based interface for
     * running a game.
     */
    class TextEngine: public Engine
    {
        protected:

        /**
         * Prints the game content (date, notifications, map)
         * onto the screen.
         */
        void printGame();

        /**
         * Prints the map content on the screen.
         */
        void printMap();

        public:

        /**
         * @see Engine::run
         */
        virtual void run();
    };
}


#endif
