/*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2024  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef YOURCITY__ENGINE_H
#define YOURCITY__ENGINE_H


#include <memory>


#include "Game.h"


namespace YourCity
{
    /**
     * The Engine abstract class and its complete implementations
     * are responsible for controlling a Game instance.
     * An Engine class does the interfacing between the game and the user.
     */
    class Engine
    {
        protected:

        /**
         * The game to be controlled by the engine.
         */
        std::shared_ptr<Game> game;

        public:

        virtual void setGame(std::shared_ptr<Game> game)
        {
            this->game = game;
        }

        /**
         * Starts the engine and then starts running the game.
         *
         * This method shall only return when a fatal error has
         * occurred that prevents the game from running or when
         * the user has requested the game to end.
         */
        virtual void run() = 0;
    };
}


#endif
