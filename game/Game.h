/*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2024  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef YOURCITY__GAME_H
#define YOURCITY__GAME_H


#include <memory>
#include <string>


#include <fmt/core.h>


#include "../core/Map.h"


namespace YourCity
{
    class Game
    {
        protected:

        /**
         * The day part of the date in the game.
         */
        uint8_t day = 1;

        /**
         * The month part of the date in the game.
         */
        uint8_t month = 1;

        /**
         * The year part of the date in the game.
         */
        uint16_t year = 1900;

        /**
         * Statistics counter for the growth of the city for the
         * current month and the last two months.
         */
        uint32_t growth_per_month[3] = {0, 0, 0};

        /**
         * The map for the game.
         */
        std::shared_ptr<Map> map;

        public:

        /**
         * Returns the map in use for the game.
         */
        std::shared_ptr<Map> getMap()
        {
            return this->map;
        }

        /**
         * Sets the map to use for the game.
         */
        void setMap(std::shared_ptr<Map> map)
        {
            this->map = map;
        }

        std::string getDateString()
        {
            return fmt::format("{0}-{1:02d}-{2:02d}", this->year, this->month, this->day);
        }

        std::vector<std::string> getNotifications()
        {
            //TODO
            return {
                "Citizens want the mayor to know that the game is not developed enough to be playable!"
            };
        }

        /**
         * Iterates the game one cycle forward.
         */
        void iterate()
        {
            //Increase the date at the end of the iteration:
            //Early development stage: Assume 30 days per month
            //and 360 days per year.
            this->day++;
            if (this->day > 30) {
                this->day = 1;
                this->month++;
            }
            if (this->month > 12) {
                this->month = 1;
                this->year++;
            }
        }
    };
}


#endif
