/*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2024  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "TextEngine.h"


using namespace YourCity;


void TextEngine::printGame()
{
    if (!this->game) {
        //Nothing to print.
        return;
    }
    auto date = this->game->getDateString();
    std::cout << date << ":\n" << "-----------\n";

    auto notifications = this->game->getNotifications();
    if (!notifications.empty()) {
        std::cout << "You have messages:\n";
        for (auto notification: notifications) {
            std::cout << "- " << notification << "\n";
        }
        std::cout << "\n";
    }

    this->printMap();
}


void TextEngine::printMap()
{
    if (!this->game) {
        //Nothing to print.
        return;
    }
    auto map = this->game->getMap();
    if (!map) {
        //Still nothing to print.
        return;
    }

    //Output the map:

    auto width  = map->getWidth();
    auto length = map->getLength();
    auto tiles  = map->getTilesInRectangle(
        Position(0,0),
        Position(width - 1, length - 1)
        );

    std::string output = "";
    for (uint16_t k = 0; k < width; k++) {
        output += "+-------";
    }
    output += "+\n";

    uint16_t i = 0;
    uint16_t j = 0;
    for (auto tile: tiles) {
        if (tile->zone_type == ZoneType::NONE) {
            output += "|       ";
        } else if (tile->zone_type == ZoneType::MUNICIPAL) {
            output += "| M     ";
        } else if (tile->zone_type == ZoneType::RESIDENTIAL) {
            if (tile->building) {
                output += "| R     ";
            } else {
                output += "| r     ";
            }
        } else if (tile->zone_type == ZoneType::COMMERCIAL) {
            if (tile->building) {
                output += "| C     ";
            } else {
                output += "| c     ";
            }
        } else if (tile->zone_type == ZoneType::INDUSTRIAL) {
            if (tile->building) {
                output += "| I     ";
            } else {
                output += "| i     ";
            }
        } else if (tile->zone_type == ZoneType::WASTELAND) {
            if (tile->building) {
                output += "| W     ";
            } else {
                output += "| w     ";
            }
        } else if (tile->zone_type == ZoneType::AIRPORT) {
            if (tile->building) {
                output += "| A     ";
            } else {
                output += "| a     ";
            }
        } else if (tile->zone_type == ZoneType::HARBOR) {
            if (tile->building) {
                output += "| H     ";
            } else {
                output += "| h     ";
            }
        }
        i++;
        if (i >= width) {
            output += "|\n";
            for (uint16_t k = 0; k < width; k++) {
                output += "+-------";
            }
            output += "+\n";
            i = 0;
            j++;
        }
    }
    std::cout << output << "\n";
}


void TextEngine::run()
{
    if (!this->game) {
        std::cerr << "No game was loaded!\n";
        return;
    }

    this->printGame();

    std::string last_command;
    std::string command;

    std::cout << "> ";
    std::getline(std::cin, command);

    while (last_command == "" && command != "exit") {
        if (command == "") {
            this->game->iterate();
            this->printGame();
        } else {
            //Handle commands.
            std::cout << "No commands available yet.\n";
        }
        std::cout << "> ";
        std::getline(std::cin, command);
    }
}
