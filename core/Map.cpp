/*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2024  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "Map.h"


using namespace YourCity;


Map::Map(uint16_t width, uint16_t length, int8_t height)
    : width(width),
      length(length)
{
    this->tiles.resize(width*length);
    for (uint64_t i = 0; i < width*length; i++) {
        this->tiles[i] = std::make_shared<Tile>(height);
    }
}


Map::Map(uint16_t width, uint16_t length, std::vector<int8_t> height_map)
    : width(width),
      length(length)
{
    size_t tile_amount = width * length;
    this->tiles.resize(tile_amount);
    size_t i = 0;
    for (auto height: height_map) {
        if (i < tile_amount) {
            this->tiles[i] = std::make_shared<Tile>(height);
        } else {
            //Out of range
            break;
        }
        i++;
    }
    if (i < tile_amount) {
        //height_map had less elements than needed to fill out the
        //whole map. Fill the rest of the map with zero height tiles.
        while (i < tile_amount) {
            this->tiles[i] = std::make_shared<Tile>(0);
            i++;
        }
    }
}


uint16_t Map::getWidth()
{
    return this->width;
}


uint16_t Map::getLength()
{
    return this->length;
}


void Map::iterate()
{
    
}


bool Map::onTheMap(const Position& position) const
{
    return (position.northeast < this->width)
        && (position.northwest < this->length);
}


uint32_t Map::positionToIndex(const Position& position) const
{
    if (!this->onTheMap(position)) {
        throw std::out_of_range("Position outside of map!");
    }
    return this->width * position.northeast + position.northwest;
}


std::vector<Position> Map::getSurroundingTilePositions(const Position& position) const
{
    //TODO!
    return {};
}


std::vector<std::shared_ptr<Tile>> Map::getTilesInLine(
    const Position& position_start,
    const Position& position_end
    )
{
    if (!this->onTheMap(position_start) || !this->onTheMap(position_end)) {
        throw std::out_of_range("Positions outside of map!");
    }

    if (position_start == position_end) {
        //This is a vector with only one tile.
        return {
            this->query(position_start)
        };
    } else {
        //STUB: Only lines following one of the both axis
        //are supported.
        std::vector<std::shared_ptr<Tile>> tiles;
        if (position_start.northeast == position_end.northeast) {
            Position current = position_start;
            if (position_start.northwest >= position_end.northwest) {
                while (current.northwest >= position_end.northwest) {
                    auto tile = this->query(current);
                    if (tile != nullptr) {
                        tiles.push_back(tile);
                    }
                    current.northwest--;
                }
            } else {
                while (current.northwest <= position_end.northwest) {
                    auto tile = this->query(current);
                    if (tile != nullptr) {
                        tiles.push_back(tile);
                    }
                    current.northwest++;
                }
            }
        } else if (position_start.northwest == position_end.northwest) {
            Position current = position_start;
            if (position_start.northeast >= position_end.northeast) {
                while (current.northeast >= position_end.northeast) {
                    auto tile = this->query(current);
                    if (tile != nullptr) {
                        tiles.push_back(tile);
                    }
                    current.northeast--;
                }
            } else {
                while (current.northeast <= position_end.northeast) {
                    auto tile = this->query(current);
                    if (tile != nullptr) {
                        tiles.push_back(tile);
                    }
                    current.northeast++;
                }
            }
        } else {
            //Not yet implemented.
            return {};
        }

        //Code to implement:
        //Follow the line from position_start to position_end.
        //Increment/decrement northeast in one step and then
        //increment/decrement northwest in the following step
        //until position_end is reached.
        Position current = position_start;
        bool increment_northeast = false;
        bool increment_northwest = false;
        if (position_start.northeast < position_end.northeast) {
            increment_northeast = true;
        }
        if (position_start.northwest < position_end.northwest) {
            increment_northwest = true;
        }
        while (current.northwest != position_end.northwest) {
            while (current.northeast != position_end.northeast) {
                if (increment_northeast) {
                    current.northeast++;
                } else {
                    current.northeast--;
                }
            }
            if (increment_northwest) {
                current.northwest++;
            } else {
                current.northwest--;
            }
        }
    }
    return {};
}


std::vector<std::shared_ptr<Tile>> Map::getTilesInRectangle(
    const Position& position_start,
    const Position& position_end
    )
{
    if (!this->onTheMap(position_start) || !this->onTheMap(position_end)) {
        throw std::out_of_range("Positions outside of map!");
    }

    if (position_start == position_end) {
        //This is a vector with only one tile.
        return {
            this->query(position_start)
        };
    } else {
        //Calculate the northernmost and the soutrhernmost position
        //by the start and end position. The start position can lie
        //in the west and the end position can be in the east. Therefore
        //it is necessary to convert the positions to "standard" positions.

        //The northernmost position has the lowest northeast and the
        //lowest northwest coordinates of the both points.
        //For the southernmost position, it is the opposite.
        Position north_position = position_start;
        if (position_start.northeast > position_end.northeast) {
            north_position.northeast = position_end.northeast;
        }
        if (position_start.northwest > position_end.northwest) {
            north_position.northwest = position_end.northwest;
        }
        Position south_position = position_end;
        if (position_start.northeast > position_end.northeast) {
            south_position.northeast = position_start.northeast;
        }
        if (position_start.northwest > position_end.northwest) {
            south_position.northwest = position_start.northwest;
        }

        //Now get all the tiles in that rectangle.
        std::vector<std::shared_ptr<Tile>> tiles;

        Position current = north_position;
        while (current.northeast <= south_position.northeast) {
            while (current.northwest <= south_position.northwest) {
                auto tile = this->query(current);
                if (tile != nullptr) {
                    tiles.push_back(tile);
                }
                current.northwest++;
            }
            current.northwest = north_position.northwest;
            current.northeast++;
        }
        return tiles;
    }
}


void Map::setZoneType(
    const Position& position_start,
    const Position& position_end,
    const ZoneType& zone_type
    )
{
    if (position_start == position_end) {
        //Set the zone type for just one tile.
        auto tile = this->query(position_start);
        tile->zone_type = zone_type;
    } else {
        auto tiles = this->getTilesInRectangle(position_start, position_end);
        for (auto tile: tiles) {
            tile->zone_type = zone_type;
        }
    }
}


void Map::setSurfaceTraffic(
    const Position& position_start,
    const Position& position_end,
    const SurfaceTrafficType& traffic_type
    )
{
    if (position_start == position_end) {
        //Set the surface traffic type for just one tile.
        auto tile = this->query(position_start);
        tile->surface_traffic_type = traffic_type;
    } else {
        auto tiles = this->getTilesInLine(position_start, position_end);
        for (auto tile: tiles) {
            tile->surface_traffic_type = traffic_type;
        }
    }
}


std::shared_ptr<Tile> Map::query(const Position& position)
{
    uint32_t index = this->positionToIndex(position);
    return tiles.at(index);
}


std::string Map::toCsv()
{
    std::string output = "";

    auto current = Position(0,0);

    for (auto tile: this->tiles) {
        output += "\"";
        if (tile == nullptr) {
            output += "0";
        } else {
            bool tile_output = false;
            bool tile_has_surface_traffic = false;
            if (tile->surface_traffic_type != SurfaceTrafficType::NONE) {
                if (tile_output) {
                    output += ":";
                }
                switch (tile->surface_traffic_type) {
                    case SurfaceTrafficType::STREET:
                    {
                        output += "stS";
                        break;
                    }
                    case SurfaceTrafficType::RAILWAY:
                    {
                        output += "stR";
                    }
                }
                tile_output = true;
                tile_has_surface_traffic = true;
            }
            if (tile->underground_traffic_type != UndergroundTrafficType::NONE) {
                if (tile->underground_traffic_type == UndergroundTrafficType::SUBWAY) {
                    if (tile_output) {
                        output += ":";
                    }
                    output += "utS";
                }
                tile_output = true;
            }
            if (!tile_has_surface_traffic) {
                if (tile_output) {
                    output += ":";
                }
                switch (tile->zone_type) {
                    case ZoneType::NONE:
                    {
                        output += "-";
                        break;
                    }
                    case ZoneType::MUNICIPAL:
                    {
                        output += "zM";
                        break;
                    }
                    case ZoneType::RESIDENTIAL:
                    {
                        output += "zR";
                        break;
                    }
                    case ZoneType::COMMERCIAL:
                    {
                        output += "zC";
                        break;
                    }
                    case ZoneType::INDUSTRIAL:
                    {
                        output += "zI";
                        break;
                    }
                    case ZoneType::WASTELAND:
                    {
                        output += "zW";
                        break;
                    }
                    case ZoneType::AIRPORT:
                    {
                        output += "zA";
                        break;
                    }
                    case ZoneType::HARBOR:
                    {
                        output += "zH";
                        break;
                    }
                }
            }
        }
        output += "\";";
        current.northwest++;
        if (current.northwest >= this->length) {
            output += "\n";
            current.northwest = 0;
            current.northeast++;
        }
        if (current.northeast >= this->width) {
            output += "\n";
            return output;
        }
    }
    return output;
}
