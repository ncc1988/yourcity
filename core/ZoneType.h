/*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2020  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef YOURCITY_ZONETYPE_H
#define YOURCITY_ZONETYPE_H


namespace YourCity
{
    /**
     * An enum defining the different types a tile can be assigned to.
     */
    enum class ZoneType
    {
        /**
         * The tile is unused.
         */
        NONE,

        /**
         * The tile is part of a municipal building like the city hall,
         * police station, fire department, hospital, schools, ...
         */
        MUNICIPAL,

        /**
         * The tile is for residential buildings.
         */
        RESIDENTIAL,

        /**
         * The tile is for commercial buildings.
         */
        COMMERCIAL,

        /**
         * The tile is for industrial buildings.
         */
        INDUSTRIAL,

        /**
         * The tile is for wasteland.
         */
        WASTELAND,

        /**
         * The tile is for airport buildings or areas.
         */
        AIRPORT,

        /**
         * The tile is for harbors.
         */
        HARBOR


        //More types to come?
    };
}


#endif
