/*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2024  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef YOURCITY_MAP_H
#define YOURCITY_MAP_H


#include <cstdint>
#include <memory>
#include <stdexcept>
#include <vector>


#include "Position.h"
#include "Tile.h"
#include "ZoneType.h"




namespace YourCity
{
    /**
     * The Map class manages all tiles of a city.
     * It is responsible for calculations of the tile's parameters for
     * the next iteration step of the city.
     * Furthermore it has a registry of all buildings of the city.
     *
     * The map starts from the northernmost point.
     * Positions are expressed as distance from the nortwestern and northeastern
     * edge of the map, where the northernmost point has the coordinates
     * northeast = 0 and northwest = 0. The northeastern edge of the map has the
     * northeast coordinate set to 0.
     *
     */
    class Map
    {
        protected:


        /**
         * The map width (width of the northeast edge).
         */
        uint16_t width = 0;


        /**
         * The map length (width of the northwest edge).
         */
        uint16_t length = 0;


        /**
         * This vector contains all tiles of the map in the current
         * iteration step.
         */
        std::vector<std::shared_ptr<Tile>> tiles;


        /**
         * This vector contains all tiles of the map in the next iteration step.
         * It has exactly the same size as the tiles vector and is only used
         * during the iterate method.
         */
        std::vector<std::shared_ptr<Tile>> next_tiles;


        public:


        /**
         * Generates a new flat map.
         */
        Map(uint16_t width, uint16_t length, int8_t height = 0);


        /**
         * Generates a new map using a height map.
         * The height map should contain width x length elements.
         * If more elements are in the height map, the rest of them are ignored.
         * In case the height map has less than width x length elements, the
         * remaining tiles will have a height of 0 (minimum land level).
         */
        Map(uint16_t width, uint16_t length, std::vector<int8_t> height_map = {});

        /**
         * @returns uint16_t The width of the map in tiles.
         */
        uint16_t getWidth();

        /**
         * @returns uint16_t The length of the map in tiles.
         */
        uint16_t getLength();


        /**
         * Iterates the tiles. This means recalculating their values like crime,
         * pollution, attractivity etc. by the values of surrounding tiles.
         */
        void iterate();


        /**
         * @returns True, if the position lies inside of the
         *     map's boundaries, false otherwise.
         */
        bool onTheMap(const Position& position) const;


        /**
         * Calculates the tile index for a position.
         * The formula to get the index from a position is:
         * width * northeast + northwest;
         *
         * @throws std::out_of_range If one of the coordinates lies outside
         *     of the map.
         */
        uint32_t positionToIndex(const Position& position) const;


        /**
         * Calculates the positions for the tiles surrounding a tile,
         * specified by its position. The indices
         */
        std::vector<Position> getSurroundingTilePositions(const Position& position) const;


        /**
         * Retrieves all the tiles that are in the line
         * defined by two positions.
         *
         * @param const Position& position_start The start
         *     coordinate of the line.
         *
         * @param const Position& position_end The end
         *     coordinate of the line.
         *
         * @throws std::out_of_range In case at least one of
         *     the both positions lie outside of the map.
         */
        std::vector<std::shared_ptr<Tile>> getTilesInLine(
            const Position& position_start,
            const Position& position_end
            );


        /**
         * Retrieves all the tiles that are in the rectangle
         * defined by two positions.
         *
         * @param const Position& position_start The first
         *     coordinate spanning the rectangle.
         *
         * @param const Position& position_end The second
         *     coordinate spanning the rectangle.
         *
         * @throws std::out_of_range In case at least one of
         *     the both positions lie outside of the map.
         */
        std::vector<std::shared_ptr<Tile>> getTilesInRectangle(
            const Position& position_start,
            const Position& position_end
            );

        /**
         * Sets the zone type for a rectangle of tiles, if possible.
         *
         * @throws std::out_of_range If the coordinates lie outside of the
         *       map.
         * TODO: Throws OccupiedException, if the tile is already occupied
         *       with a building or a traffic way.
         * TODO: Throws FloodedException if the tile is below sea level.
         */
        void setZoneType(
            const Position& position_start,
            const Position& position_end,
            const ZoneType& zone_type
            );


        /**
         * Sets the surface traffic type for a line of tiles, if possible.
         *
         * @throws std::out_of_range If the coordinates lie outside of the
         *       map.
         */
        void setSurfaceTraffic(
            const Position& position_start,
            const Position& position_end,
            const SurfaceTrafficType& traffic_type
            );


        /**
         * Retrieves a tile to "query" it (get all the information of the tile).
         *
         * @param uint16_t northeast The offset from the northeast edge.
         *
         * @param uint16_t northwest The offset from the northwest edge.
         *
         * @returns std::shared_ptr<Tile> The tile for the specified position.
         *
         * @throws std::out_of_range, if the coordinates lie outside of the
         *     map.
         */
        std::shared_ptr<Tile> query(const Position& position);



        /**
         * Converts the map and its tiles to CSV data.
         * A semicolon is used as column separator and a colon
         * separates tile data.
         */
        std::string toCsv();
    };
}


#endif
