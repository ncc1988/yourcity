/*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2024  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef YOURCITY_TILE_H
#define YOURCITY_TILE_H


#include <cstdint>
#include <memory>


#include "ZoneType.h"
#include "traffic/SurfaceTrafficType.h"
#include "traffic/UndergroundTrafficType.h"
#include "Building.h"


namespace YourCity
{
    class Tile
    {
        public:


        Tile(int8_t height = 0);


        /**
         * The height of the tile. A value of 0 means 0 meter above sea level
         * which is the lowest possible land level.
         */
        int8_t height = 0;


        /**
         * The zone type that is assigned to the tile.
         */
        ZoneType zone_type = ZoneType::NONE;


        /**
         * The type of transportation system that is built on the tile.
         */
        SurfaceTrafficType surface_traffic_type = SurfaceTrafficType::NONE;


        /**
         * The type of trasportation system that is built below the tile.
         */
        UndergroundTrafficType underground_traffic_type = UndergroundTrafficType::NONE;


        /**
         * The building that is built on the tile.
         * A null pointer indicates that no building is built on the tile.
         */
        std::shared_ptr<Building> building = nullptr;


        /**
         * The tile number of the building. If a building bigger than 1x1 tile
         * is placed on the tile, its position "inside" the building must be
         * stored to calculate the building's position. The tile position zero
         * represents the most northwest tile of the building. From there, the
         * position number moves to the east until the end of the "row"
         * is reached. It then continues with the next southern row, starting
         * in the most western tile again.
         */
        uint8_t building_tile_position = 0;

        /**
         * Whether the tile is contaminated with radioactive material (true)
         * or not (false).
         */
        bool radioactive = false;


        //TODO: overloaded assignment operator etc.
    };
}


#endif
