/*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2025  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef YOURCITY__BUILDINGSTATE_H
#define YOURCITY__BUILDINGSTATE_H


namespace YourCity
{
    /**
     * An enum class for the states the building can be in.
     */
    enum class BuildingState
    {
        /**
         * The building is under construction.
         */
        CONSTRUCTION,

        /**
         * The building is in use.
         */
        IN_USE,

        /**
         * The building is abanoned.
         */
        ABANDONED,

        /**
         * The building has been destroyed.
         */
        DESTROYED,

        /**
         * The building burned down.
         */
        BURNED
    };
}


#endif
