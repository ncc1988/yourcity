/*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2021  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


namespace YourCity
{
    class Position
    {
        public:


        uint16_t northeast = 0;


        uint16_t northwest = 0;


        Position(uint16_t northeast = 0, uint16_t northwest = 0)
            : northeast(northeast),
              northwest(northwest)
        {
        }


        bool operator==(const Position& other) const
        {
            return (this->northeast == other.northeast)
                && (this->northwest == other.northwest);
        }
    };
}
