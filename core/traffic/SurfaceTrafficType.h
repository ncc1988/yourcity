/*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2025  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef YOURCITY__SURFACETRAFFICTYPE_H
#define YOURCITY__SURFACETRAFFICTYPE_H


namespace YourCity
{
    /**
     * An enum defining the different traffic types that can be built on a tile.
     */
    enum class SurfaceTrafficType
    {
        /**
         * No traffic.
         */
        NONE,

        /**
         * A street tile.
         */
        STREET,

        /**
         * A railway tile.
         */
        RAILWAY,

        /**
         * A motorway tile.
         */
        MOTORWAY

        //More types to come? Maglev?
    };
}


#endif
