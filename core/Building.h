/*
 *   This file is part of YourCity - a city simulation
 *   Copyright (C) 2014-2025  Moritz Strohm <ncc1988@posteo.de>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef YOURCITY_BUILDING_H
#define YOURCITY_BUILDING_H


#include <string>


#include "buildings/BuildingState.h"


namespace YourCity
{
    class Building
    {
        protected:

        /**
         * The state of the building.
         */
        BuildingState state = BuildingState::CONSTRUCTION;

        /**
         * The age of the building in game days.
         */
        uint64_t age = 0;

        public:

        /**
         * @returns BuildingState The state of the building.
         */
        BuildingState getState()
        {
            return this->state;
        }

        /**
         * Sets the state of the building.
         *
         * @param BuildingState state The state of the building to be set.
         */
        void setState(BuildingState state)
        {
            this->state = state;
        }

        /**
         * @returns uint64_t The age of the building in game days.
         */
        uint64_t getAge()
        {
            return this->age;
        }
    };
}


#endif
