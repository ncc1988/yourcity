#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of YourCity
#  Copyright (C) 2014 Moritz Strohm <ncc1988@posteo.de>

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


from fife import fife
from fife.extensions.fife_settings import Setting

from game.YourCity import YourCity


settings = Setting(app_name="yourcity",
  settings_file="./settings.xml"#,
  #settings_gui_xml=""
  )

def main():
  app = YourCity(settings)
  app.run()
  
if __name__ == '__main__':
  main()
