
from fife import fife

class YourCityGameController(object):
  def __init__(self, application, engine, settings):
    self.application = application
    self.engine = engine
    self.model = self.engine.getModel()
    self.settings = settings

    self.gameListener = GameListener(self.engine, self)

  def pump(self):
    pass