#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  This file is part of YourCity
#  Copyright (C) 2014 Moritz Strohm <ncc1988@posteo.de>

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


from fife import fife

from fife.extensions import pychan
from fife.extensions.basicapplication import ApplicationBase

from game.YourCityMouse import YourCityMouseListener
from gui.SimpleDialog import SimpleDialog



class YourCity(ApplicationBase):
  
  def __init__(self, loadedSettings):
    super(YourCity,self).__init__(loadedSettings)
    
    self._settings = loadedSettings
    
    self._gameState = 0 #0 = menu, 1 = in game
    self._gamePaused = True;
    self._gameSpeed = 1 #0 = pause, 1="snail", 2="turtle", 3="swallow", 4="gepard"
    
    self._eventManager = self.engine.getEventManager()
    self._mouseListener = YourCityMouseListener(self)
    self._eventManager.addMouseListenerFront(self._mouseListener)
    
    #we need something that does the map loading for us: a MapLoader instance
    self._mapLoader = fife.MapLoader(
      self.engine.getModel(), 
      self.engine.getVFS(), 
      self.engine.getImageManager(), 
      self.engine.getRenderBackend()
      )

    
    #create the main menu GUI from the XML definition file:
    self._mainMenu = pychan.loadXML('gui/MainMenu.xml')
    #...and attach events to GUI elements:
    self._mainMenu.mapEvents({
      'startGameButton': self.startGame,
      'exitButton' : self.onExit,
      })
    self._mainMenu.show()
    
    #add dialogs:
    self._quitDialog = SimpleDialog(self.onExitConfirm, self.onExitAbort, "Exit game", "Are you sure you want to quit?", "Yes", "No")
    self._saveDialog = SimpleDialog(self.onExitSave, self.onExitConfirm, "Unsaved game", "Do you want to save this game before quitting?", "Yes", "No")
  
  def loadDevelopmentMap(self, fileName):
    if self._mapLoader.isLoadable(fileName):
      self._loadedMap = self._mapLoader.load(fileName)
      self._Camera = self._loadedMap.getCamera("Camera")
      self._GroundLayer = self._loadedMap.getLayer("Ground")
      self._BuildingsLayer = self._loadedMap.getLayer("Buildings")
      
      #we have to get each cell of the ground and buildings layer so that we can find out
      #about buildings etc.:
      #self._GroundLayerCells = self._GroundLayer.getCellCache()
      #self._BuildingsLayerCells = self._BuildingsLayer.getCellCache()
    else:
      print("YourCity: Error: Map "+ fileName +" not loadable!")
  
  
  def startGame(self):
    self._mainMenu.findChild(name="startGameButton").hide() # we don't need this button anymore
    self._gameState = 1 #in game
    self.loadDevelopmentMap("./res/dev/devmap1.xml")
  
  
  def onExit(self):
    if self._gameState == 1: #in game: ask if the game should be saved
      self._gamePaused = True
      self._saveDialog.show()
    else: #in main menu: ask if the user really wants to quit
      self._quitDialog.show()
      
  def onExitConfirm(self):
    print("YourCity: onExitConfirm called!")
    #call exit function
    self.quitGame()
    
  def onExitSave(self):
    print("YourCity: onExitSave called!")
    #save game (call extra function) and then the exit function
  
  def onExitAbort(self):
    print("YourCity: onExitAbort called!")
    if self._gameState == 1:
      self._gamePaused = False
  
  def quitGame(self):
    #cleanup:
    #(TODO)
    
    #quit:
    self.quit()
    
  def _pump(self):
    pass
    
  def handleMousePress(self, screenPoint):
    if self._gameState == 1:
      #in game and not a click on the menu: the user clicked on the map:
      #we have to get the tile and the tool the user uses at the moment
      
      #STUB: info tool selected
      target_mapcoord = self._Camera.toMapCoordinates(screenPoint, False)
      target_mapcoord.z = 0 #no height in this game
      location = fife.Location(self._BuildingsLayer)
      location.setMapCoordinates(target_mapcoord)
      selectedInstance = self._BuildingsLayer.getInstanceAt(location, False)
      #get the cell the user clicked on:
      #selectedCell = self._BuildingsLayerCells.getCell(self, location)
      print("Clicked on a cell!")
      print("Information:")
      print("-------------")
      print("X: " + location.getX() + ", Y: " + location.getY())
      print("-------------")
    pass
