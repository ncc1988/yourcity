This folder contains artwork for YourCity.


The content of this folder is licensed under the terms of the Creative Commons 
Attribution-ShareAlike 4.0 International License (CC BY-SA 4.0), unless otherwise noted.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or
send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
