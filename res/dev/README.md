This folder contains blender files to generate tiles in different sizes.

The file tile-base.blend is derived from the article "Isometric Tiles in Blender"
by Clint Bellanger who kindly published the article under the Creative Commons
Attribution - Share Alike license. The original article can be found here:

http://clintbellanger.net/articles/isometric_tiles/

Contrary to the original, the tile-base.blend file has four cameras and doesn't
use a script for rendering the tile.


## tile-base.blend ##

This file is useful for flat tiles. The rendered resoulution may be enough for
1x1 tiles but it should be doubled for 2x2 tiles and so on.

There are four cameras in the file, representing each view direction
(north, south, east, west). Note that each tile has to be rendered 4 times
to have a rendered image for each direction.
